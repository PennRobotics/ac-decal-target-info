Target Info
===========

_Target Info_ is a _Decal_ plugin for a discontinued MMORPG, _Asheron&rsquo;s Call_ (AC).

> [!NOTE]
> I am not a developer of Target Info, Decal, or AC.

-----

## Troubleshooting

### Current Problem

```text
<{ Target Info v3.1.0.1 }> Error #445 occurred in WorldFilter_ChangeObject: Object doesn't support this action @ line 1412
```

There are no search results for the error code (#445) nor the error string ("support this action").
It is built in as a Visual Basic error:
[related VB doc](https://learn.microsoft.com/en-us/office/vba/language/reference/user-interface-help/object-doesn-t-support-this-action-error-445)

- Check spelling of properties/methods.
- Read documentation about the object.

```vb
1412  Set VulnDbase.dctIDedCache(tmpMonster.MonsterName) = tmpMonster.VulnInfo
```

`tmpMonster` exists. `tmpMonster.VulnInfo.HasVulnInfo` is **false**. `VulnDbase.dctIDedCache.Exists(tmpMonster.MonsterName)` is **false**.

In any case, the source will probably need to be recompiled as this does not seem like a filename error. Plus, it will be easier to see the variable members and how they are defined by adding a few debug statements.

Still, it makes sense to see how `tmpMonster` and `VulnDbase` are defined, since one of these is the root of the current error. It does not make sense that this plugin&mdash;which previously should have worked&mdash;has suddenly stopped functioning despite no internal changes. That basically leaves external dependencies: XML files, XML parser, missing DLL, a missing flag/.ini, server needs to be up, AC emulator has a different monster ID system than the original retail server, etc.

### Details

```vb
Private Sub WorldFilter_ChangeObject(ByVal pObject As DecalFilters.IWorldObject, ByVal Change As DecalFilters.WorldChangeType)
1388      On Error GoTo erh
1390      If Change = wevtIdentReceived Then
              Dim wObj As WorldObject, tmpMonster As clsMonster
1392          Set wObj = pObject

1394          If dctVulMonsters.Exists(wObj.GUID) Then
1396              Set tmpMonster = dctVulMonsters(wObj.GUID)
1398          ElseIf dctNonVulMonsters.Exists(wObj.GUID) Then
1400              Set tmpMonster = dctNonVulMonsters(wObj.GUID)
1402          End If

1404          If Not tmpMonster Is Nothing Then
1406              If Not tmpMonster.VulnInfo.HasVulnInfo And tmpMonster.MonsterType = eMonster Then
1408                  If Not VulnDbase.dctIDedCache.Exists(tmpMonster.MonsterName) Then
1410                      Set tmpMonster.VulnInfo = VulnDbase(wObj.name, wObj.Longs(keySpecies))
1412                      Set VulnDbase.dctIDedCache(tmpMonster.MonsterName) = tmpMonster.VulnInfo
1414                      If tmpMonster.VulnInfo.HasVulnInfo Then
1416                          wtcwMessage "Species vuln info used for: «2»" & wObj.name
1418                      Else
                              'wtcwWarning "No vuln info available for: «2»" & wObj.Name
1420                      End If
1422                      VulnDbase.SaveCache
1424                  End If
1426              End If
1428          End If

1430          Set tmpMonster = Nothing

1432      End If
1434      Exit Sub
erh:
1436      handleErr "WorldFilter_ChangeObject"
End Sub
```

Here is a small part of the `clsMonster` structure used to define `tmpMonster`:

```vb
2074          Set VulnInfo = VulnDbase(MonsterName, wObj.Longs(keySpecies, -1))
2076          If Not VulnInfo.HasVulnInfo And Not wObj.HasIdData Then
                  'Try to get species data
2078              PluginSite2.Hooks.RequestID MonsterGUID
2080          End If
```

I assume `MonsterName` is defined correctly, but I imagine `PluginSite2.Hooks.RequestID` could be faulty, or `VulnDbase` is poorly defined.

Here is a snippet from `VulnDbase`:

```vb
Public Sub Load()
3012      On Error GoTo erh
          Dim xmlDoc As New DOMDocument40, xmlNode As IXMLDOMElement
          Dim VulnInfo As clsVulnInfo
          Dim Vuln

3014      dctCreatures.removeAll
3016      dctSpecies.removeAll
3018      dctIDedCache.removeAll

3020      If xmlDoc.Load(App.Path & "\vuln_idcache.xml") Then
3022          For Each xmlNode In xmlDoc.selectNodes("//resists/creature")
3024              Set VulnInfo = New clsVulnInfo
3026              For Each Vuln In Data.arrDbVulnNames
   ...
3050      End If

3052      If xmlDoc.Load(App.Path & "\vuln_database.xml") Then

3054          For Each xmlNode In xmlDoc.selectNodes("//resists/species")
   ...
3112      Else
3114          wtcwError "Failed to load vuln database: «2»" & App.Path & "\vuln_database.xml"
3116      End If

3118      UpdateCtText
```

We might be able to elicit an error by creating an XML file, **vuln_idcache.xml**, and putting a valid node with missing attributes to fail on lines 3036 thru 3040.

Other XMLs and filenames found in the source:

- **unparsed_database.xml**
- **settings.xml** (Notably, this file is _not_ being created. Why?)
- **errors.txt**

What happens, then, using `WINEDEBUG=+file`? Could it be that `App.Path` is not defined correctly? Or does the install directory have no write permissions?

<mark>Update</mark>: In fact, there were two installations of the plugin. THe second directory was being populated. It also contains an error at the start of each session inside of **errors.txt**:

```text
Error #445 occurred in Class_Initialize: Object doesn't support this action @ line 3200
```

What is at line 3200?

```vb
Private Sub Class_Initialize()
3192      On Error GoTo erh
3194      dctCreatures.CompareMode = TextCompare
3196      dctSpecies.CompareMode = TextCompare
3198      dctIDedCache.CompareMode = TextCompare
3200      Load
3202      Exit Sub
erh:
3204      handleErr "Class_Initialize"
End Sub
```

`Load`? But `Load` has its own error handler, right?

Just for shits and giggles, I created an empty **vuln_idcache.xml**, as there seems to be no good reason why `Load` causes an error while it has its own handler. This did not help.



-----

Target Info provides a heads up display (HUD) with information about an opponent, its relative strengths and weaknesses, and any effects that might temporarily impair or toughen it. It relies on a local database that is synchronized through a web server.

-----

Decal is a standalone manager that facilitates the development of plugins. As the game engine changes (in particular, memory locations of specific elements) the Decal team locates new functionality and exposes it using a message format that remains unchanging.

The MMORPG can still be used because of community support of emulators and hosted servers.

Because the original AC engine files are used and the new servers seem to transmit much of the same information to clients as the original servers, Decal and its plugins are mostly usable. One limitation is when a plugin needs to connect to an online database.

-----

## Checklist

- [ ] Try making an intentionally broken **vuln_idcache.xml** to determine if the failure source can be traced and/or bypassed
- [x] find a copy of the original .XML file (as downloaded from the compendium server) to see if this differs from the local file copied into the main directory during installation.
    - [x] **Here:** https://web.archive.org/web/20050101181614/http://ac.warcry.com/compendium/creature/creature_xml.php (also available in the original source)
    - [ ] what is https://raw.githubusercontent.com/Darktorizo/GoArrow_Data_CoD/master/data_cod.xml ?
    - [ ] https://ac.yotesfan.com/weenies/ ?
    - [ ] http://www.virindi.net/vstats/monsters.php ?
- [ ] XML debugging
    - [ ] see if the .XML lookup is buggy
    - [ ] see if there is a flag needed so the plugin knows to use the local file
    - [ ] XML file cannot be found or accessed?
    - [ ] XML reader is flawed?
    - [ ] server messages do not align with the XML data?
    - [ ] plugin bug?
    - [ ] something else?
- [x] once the plugin is fixed, provide the updated database online (S3 bucket? Git repo? elsewhere?) and/or information on how others can apply the fix
    - [x] should not be needed as **vuln_database.xml** is the right file

## Possibly related

- https://skunkworks.sourceforge.net/protocol/Protocol.php
- https://ac.yotesfan.com/pcap_reports/
- https://web.archive.org/web/20060703175816/http://ac.warcry.com/compendium/creature/

## What about?

- https://web.archive.org/web/20060107015807/http://www.net-marks.com/acmonster/


-----

## Current State

### Source code

https://code.google.com/archive/p/digero-ac/source/default/source?page=1 and then download as a .zip file.


### Expectation

The Target Info plugin seems to have the information it needs in its local folder for looking up an opponent and its characteristics. The HUD should display the opponent and the best (and worst) choice for its defeat.

Additionally, if the server with the necessary data is down, the user should be able to provide an alternate URL.

### Emulation

I am running this plugin in Linux via WINE.

Not mentioned in most of the documentation of this plugin is the need for **msvbvm60**, which can be installed quite easily using _winetricks_: `winetricks vb6run`

This can be discovered by noting the exit code when _regsvr32_ fails during installation, then running this command manually with `WINEDEBUG=+all` and piping _stderr_ to a file, then searching for exit code 3 and looking for the error source. (The log itself is enormous: something like 100 MB, IIRC. There is probably a better debug flag, but I found what I needed; no need to go further down the rabbithole.)

Running all of these programs in WINE is an exercise in patience and very dutifully following steps laid out by others&hellip; and having enough luck and knowledge to fill in the gaps while following the directions of someone else. For me, _Lutris_ is what finally worked, although I believe I had to switch from "8" to "7" as suggested by someone and then overwrite the local version of _winetricks_ with the current (2024) version. I still got a handful of installation errors (and these normally cause problems requiring deleting the whole attempt and starting from scratch) and I still have to tab a few times in the launcher to show its contents and move the explorer toolbar window containing the Decal icon before the icon is clickable, but I am able to launch all of the installed programs.

### Reality

Currently, the server containing information about an opponent has been taken offline. The URL seems to be hardcoded in the Target Info source. Furthermore, even though an XML file is in the Target Info directory, it fails to provide information via the HUD. The HUD itself is displayed on the screen as well as the GUI for the plugin.

There _is_ an online compendium of opponent information in a seemingly different format than needed by Target Info. I would be particularly interested in recreating the original compendium format using up-to-date information.

There are error messages provided by the plugin which should help in locating the source of the issue.

It looks like the vulnerabilities should be loaded using **vuln_database.xml**.
